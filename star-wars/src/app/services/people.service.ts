import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';
import { People } from '../model/people';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  constructor(private httpClient: HttpClient) { }

  public findAll(): Observable<any>{
    return this.httpClient.get<any>(
        environment.backendUrl + 'people/');
  }
}
