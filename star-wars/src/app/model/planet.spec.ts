import { Planet } from './planet';

describe('Planet', () => {
  it('should create an instance', () => {
    expect(new Planet('Alderaan', 24, 364, 12500, 'temperate', '1 standard' )).toBeTruthy();
  });
});
