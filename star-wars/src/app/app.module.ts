import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeopleTableComponent } from './people-table/people-table.component';
import { PlanetTableComponent } from './planet-table/planet-table.component';

@NgModule({
  declarations: [
    AppComponent,
    PeopleTableComponent,
    PlanetTableComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
