import { Component, OnInit } from '@angular/core';
import { Planet } from '../model/planet';
import { PlanetService } from '../services/planet.service';

@Component({
  selector: 'app-planet-table',
  templateUrl: './planet-table.component.html',
  styleUrls: ['./planet-table.component.css']
})
export class PlanetTableComponent implements OnInit {

  planet: Planet[];

  constructor(private planetService: PlanetService) { }

  ngOnInit() {
    this.planetService.findAll().subscribe(
      planetArray => {
        console.log(planetArray);
        this.planet = planetArray.results;
      },
      error => {
        console.log(error);
      }
    );
  }

}
