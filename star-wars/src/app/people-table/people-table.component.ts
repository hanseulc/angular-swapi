import { Component, OnInit } from '@angular/core';
import { People } from '../model/people';
import { PeopleService } from '../services/people.service';

@Component({
  selector: 'app-people-table',
  templateUrl: './people-table.component.html',
  styleUrls: ['./people-table.component.css']
})
export class PeopleTableComponent implements OnInit {

  people: People[];

  constructor(private peopleService: PeopleService) { }

  ngOnInit() {
    this.peopleService.findAll().subscribe(
      peopleArray => {
        console.log(peopleArray);
        this.people = peopleArray.results;
      },
      error => {
        console.log(error);
      }
    );
  }

}
